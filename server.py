"""
The main IDP Generator server, which performs the following:
    1. Listens at the IDP Generator GRPC endpoint
    2. Generates theory-specific protos and fires up an IDP GRPC server

Author: Simon Vandevelde <s.vandevelde@kuleuven.be>
"""
from concurrent import futures
import threading
import logging

import grpc
import idpgenerator_pb2 as pb2
import idpgenerator_pb2_grpc as pb2_grpc
from grpc_tools import protoc
from grpc_reflection.v1alpha import reflection

from theory2proto import theory_to_proto

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] %(asctime)s - %(threadName)s -  %(message)s',
                    datefmt='%d-%b-%y %H:%M:%S')
logging.basicConfig(level=logging.DEBUG)


def start_IDP_server(proto, theory, portnb):
    """ Start an IDP GRPC server based on a proto """
    with open('./protos/gen.proto', 'w') as fp:
        fp.write(proto)
    # Format arguments for protoc compiler and generate a proto
    path = ''
    idir = '-I./protos'
    out = '--python_out=.'
    grpc_out = '--grpc_python_out=.'
    name = 'gen.proto'
    protoc.main([path, idir, out, grpc_out, name])

    # Import the newly generated proto modules, and the IDP GRPC server.
    import gen_pb2_grpc
    import gen_pb2
    from IDPservice import IDP

    gen_server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    gen_pb2_grpc.add_IDPServicer_to_server(IDP(theory=theory), gen_server)
    SERVICE_NAMES = (
            gen_pb2.DESCRIPTOR.services_by_name['IDP'].full_name,
            reflection.SERVICE_NAME,
    )
    reflection.enable_server_reflection(SERVICE_NAMES, gen_server)
    logging.info(f'gen - Starting gen server at {portnb}')
    gen_server.add_insecure_port(f'[::]:{portnb}')
    gen_server.start()
    gen_server.wait_for_termination()


class IDPGenerator(pb2_grpc.IDPGenerator):
    """ Generates proto file for any theory, and launches an IDP GRPC server
    specifically for that theory """
    def __init__(self):
        self.portnb = 50051

    def generateProto(self, request, context):
        """ Based on theory, generate proto and launch IDP GRPC server """
        self.portnb += 1
        proto = theory_to_proto(request.theory)
        x = threading.Thread(target=start_IDP_server,
                             args=(proto, request.theory, self.portnb))
        x.start()
        return pb2.Proto(proto=proto, port=self.portnb)


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    pb2_grpc.add_IDPGeneratorServicer_to_server(IDPGenerator(), server)
    SERVICE_NAMES = (
            pb2.DESCRIPTOR.services_by_name['IDPGenerator'].full_name,
            reflection.SERVICE_NAME,
    )
    reflection.enable_server_reflection(SERVICE_NAMES, server)
    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig()
    logging.info('meta - Starting meta server')
    serve()
