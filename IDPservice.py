"""
IDPservice contains the IDP GRPC implementation.

Author: Simon Vandevelde <s.vandevelde@kuleuven.be>
"""
import logging
from idp_engine.API import KB

import gen_pb2
import gen_pb2_grpc


class IDP(gen_pb2_grpc.IDP):
    """ The IDP GRPC server """
    def __init__(self, theory):
        super().__init__()

        self.idp = KB()
        self.idp.from_str(theory)

    def _struct_to_message(self, struct=None, add_types=True):
        """ Convert (partial) struct into Model message.

        If no structure is given, the current IDP structure is converted into a
        message instead."""
        msg = gen_pb2.Model()

        # If no structure was given, convert IDP's current one.
        if struct is None:
            new_struct = {}
            for symbol_name, symbol in self.idp.symbols.items():
                if symbol_name in ['Bool', 'Int', 'Real', 'Date', 'Concept']:
                    continue
                symbol_type = symbol.symbol_type
                if symbol_type == 'type':
                    continue
                elif symbol_type in ['predicate', 'function']:
                    new_struct[symbol_name] = symbol.enumeration
                else:
                    new_struct[symbol_name] = symbol.value

            struct = new_struct

        # Add types to model message.
        if add_types:
            for symbol_name, symbol in self.idp.symbols.items():
                if symbol_name in ['Bool', 'Int', 'Real', 'Date', 'Concept']:
                    continue
                if (symbol.symbol_type == 'type'):
                    attr = getattr(msg, symbol_name)
                    attr.value.extend(symbol.enumeration)

        # Convert the current structure to a message.
        for symbol_name, value in struct.items():
            if value is None:
                continue
            if isinstance(value, dict):  # Function.
                for key, val in value.items():
                    if not isinstance(key, str):
                        key = ', '.join(key)
                    getattr(msg, symbol_name).value[key] = val
            elif isinstance(value, list):  # Predicate.
                for val in value:
                    if not isinstance(val, str):
                        val = ', '.join(val)
                    getattr(msg, symbol_name).value.append(val)
            else:  # Bool, Constant
                getattr(msg, symbol_name).value = value
        return msg

    def modelExpand(self, request, context):
        """ Perform model expansion """
        logging.info('gen - Model expand request')
        idp_models = self.idp.model_expand(request.nb_models)
        model_msgs = []
        for idp_model in idp_models:
            msg = gen_pb2.Model()
            logging.debug(f'gen - {idp_model}')
            msg = self._struct_to_message(idp_model)

            # Add generated model to model message.
            model_msgs.append(msg)
        return gen_pb2.Models(models=model_msgs, success=False)

    def propagate(self, request, context):
        """ Perform propagation """
        posdict, negdict = self.idp.propagate()
        posdict = self._struct_to_message(posdict, add_types=False)
        return gen_pb2.PropModels(posModel=posdict)

    def explain(self, request, context):
        """ Explain why the current structure is unsat. """
        try:
            explanation = self.idp.explain()
        except AssertionError:
            # No error was found.
            return gen_pb2.Explain(explanations=['No error found.'])

        # We also need to strip all unicode.
        explanation = explanation.replace('→', '->')
        explanation = explanation.replace('∀', '!')
        explanation = explanation.replace('∃', '?')
        explanation = explanation.replace('∈', 'in')
        explanation = explanation.replace('∉', 'not in')
        explanation = explanation.replace('←', '<-')
        explanation = explanation.replace('∧', '&')
        explanation = explanation.replace('∨', '|')
        explanation = explanation.replace('¬', '~')
        explanation = explanation.replace('⇒', '=>')
        explanation = explanation.replace('⇔', '<=>')
        explanation = explanation.replace('⇐', '<=')
        explanation = explanation.replace('≤', '=<')
        explanation = explanation.replace('≠', '~=')
        explanation = explanation.replace('≥', '>=')
        explanations = explanation.split('\n')[:-1]
        return gen_pb2.Explain(explanations=explanations)

    def getVariables(self, request, context):
        """ Returns all variables and their info.

        We need to create three dicts:
            1. Map of variables on their logical type
            2. Map of variables on their arguments used
            3. Map of variables on their return type
        """
        type_dict = {}
        arg_dict = {}
        rtype_dict = {}
        for var_name, var in self.idp.symbols.items():
            if var_name in ['Bool', 'Int', 'Real', 'Date', 'Concept']:
                continue
            if var.symbol_type == 'type':
                type_dict[var_name] = var.symbol_type
                continue
            type_dict[var_name] = var.symbol_type
            arg_dict[var_name] = ', '.join(var.arguments)
            rtype_dict[var_name] = var.rtype
        return gen_pb2.VarList(type=type_dict, args=arg_dict,
                               rtype=rtype_dict)

    def setStructure(self, request, context):
        """ Set a (partial) structure for the KB"""
        for var_name in self.idp.symbols.keys():
            if var_name in ['Bool', 'Int', 'Real', 'Date', 'Concept']:
                continue
            if not request.HasField(var_name):
                # Ignore variable if it wasn't set.
                continue
            value = getattr(request, var_name).value
            symbol_type = self.idp.symbols[var_name].symbol_type

            if symbol_type in ['predicate', 'type']:
                value = list(value)
            elif symbol_type == 'function':
                value = dict(value)
            self.idp.set_enumeration(var_name, value)

        return gen_pb2.Ok(success=True)

    def getStructure(self, request, context):
        """ Get the current (partial) structure of the KB. """
        model = self._struct_to_message()

        return model
