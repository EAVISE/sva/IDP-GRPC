"""
The IDP-Z3 API allows fluent interaction with the IDP system from inside
Python. Its goal is to allow the creation of knowledge bases on-the-fly,
in a Pythonic way.
"""
import re
from typing import Type, List, Dict, Union, Tuple  # Typing deprecated in p3.9
from idp_engine import IDP, Parse, Expression
from contextlib import redirect_stdout


class KB():
    """ Class representing a knowledge base.

    In short, such a KB is represented by three components:
        * The symbols (which go in the voc / structure)
        * The axioms (which go in the theory)
        * The definitions (which go in the theory)

    There are multiple execution methods supported:
        * model expansion
        * model optimization (min/max)
        * propagation

    When such an execution method is called, the following few steps are taken:
        1. The Pythonic KB is converted into an FO(.) KB (in string format)
        2. This is fed to the idp_engine, which performs the execution
        3. The output of the idp_engine is parsed and converted back into
            Pythonic form

    Args:
        None

    Attributes:
        symbols (`list` of `IDPSymbolObject`): a list containing the KB's
            symbols.
        axioms (`list` of `IDPConstraint`): list of KB's axioms.
        definitions (`list` of `IDPDefinition`): list of KB`s definitions.
    """
    def __init__(self):
        """ Inititalize symbol dict and definition/axioms lists.
        """
        self.symbols: Dict[str, IDPSymbolObject] = {}
        self.axioms: List[IDPConstraint] = []
        self.definitions: List[IDPDefinition] = []

        self.symbols['Bool'] = IDPStandardType('Bool')
        self.symbols['Int'] = IDPStandardType('Int')
        self.symbols['Real'] = IDPStandardType('Real')
        self.symbols['Date'] = IDPStandardType('Date')
        self.symbols['Concept'] = IDPStandardType('Concept')

    def from_str(self, theory: str) -> None:
        """ Interpret complete theory in "IDP form" at once.

        Args:
            theory (str)

        Returns:
            None
        """
        # Parse IDP, and turn it into IDP-Z3Py objects.
        parsed = IDP.from_str(theory)
        for name, voc in parsed.vocabularies.items():
            for symbol_name, symbol in voc.symbol_decls.items():
                # Skip default symbols.
                if (symbol_name in ['𝔹', 'ℤ', 'ℝ', 'Date', 'Concept', 'true',
                                    'false', '__relevant', 'arity', 'abs',
                                    'input_domain', 'output_domain']
                   or symbol_name.startswith('`')
                   or isinstance(symbol, Expression.Constructor)):
                    continue
                # If the symbol is a type, add it.
                if isinstance(symbol, Parse.TypeDeclaration):
                    if symbol.interpretation:
                        enum = list(symbol.interpretation.enumeration.tuples.values())
                    else:
                        enum = []
                    self.type(symbol_name, enum)
                else:
                    # Check if it is a predicate or a function.
                    rtype = str(symbol.out)
                    arity = symbol.arity
                    args = [str(x) for x in symbol.sorts]
                    if rtype == '𝔹' and arity > 0:
                        # Predicate
                        self.predicate(symbol_name, args)
                    elif rtype == '𝔹':
                        # Proposition
                        self.proposition(symbol_name)
                        pass
                    elif arity > 0:
                        # Function
                        self.function(symbol_name, args, rtype)
                    else:
                        # Constant
                        self.constant(symbol_name, rtype)
        # Now that the objects are created, we can add their enumerations if
        # listed in the structure.
        for name, struc in parsed.structures.items():
            for symbol_name, interpretation in struc.interpretations.items():
                if (symbol_name in ['𝔹', 'ℤ', 'ℝ', 'Date', 'Concept', 'true',
                                    'false', '__relevant', 'arity', 'abs',
                                    'input_domain', 'output_domain']
                   or symbol_name.startswith('`')):
                    continue
                symbol_type = self.symbols[symbol_name].symbol_type
                enum = list(interpretation.enumeration.tuples.values())
                if not enum:
                    if symbol_type == 'constant':
                        enum = False
                    else:
                        enum = []
                elif ',' in str(enum[0]):
                    if symbol_type == 'predicate':
                        enum = [str(x).split(',') for x in enum]
                    else:
                        enum = {}
                        for x in enum:
                            vals = x.split(',')
                            key = vals[0:-1]
                            val = vals[-1]
                            enum[key] = val
                else:
                    enum = [str(x) for x in enum]
                self.set_enumeration(symbol_name, enum)

        # Finally, iterate over the theory and add all axioms/definitions.
        for name, theo in parsed.theories.items():
            axioms = [str(x) for x in theo.constraints]
            for axiom in axioms:
                self.axiom(axiom)

            for definition in theo.definitions:
                rules = [str(x.code) for x in definition.rules]
                self.definition(rules)

    def type(self, name: str, enumeration: List[Union[int, str]]) -> None:
        """ Add new type to the KB.

        Args:
            name (str)
            enumeration (`list` of str/int): the type's domain of values.

        Returns:
            None

        Raises:
            ValueError: If a symbol already exists with the same name.
        """
        if name in self.symbols:
            raise ValueError(f'Duplicate symbol name: {name}')
        self.symbols[name] = IDPType(name, enumeration)

    def predicate(self, symbol_name: str, arguments: List[str],
                  enumeration: List[str] = []) -> None:
        """ Add new predicate to the KB.

        A predicate is defined by its name, its arguments, and (optionally) an
        enumeration. The arguments of the predicate *must* be known types,
        i.e., types that have previously been created.

        Args:
            symbol_name (str)
            arguments (`list` of str): The types of the predicate.
            enumeration (`list` of str or int, optional): Optional enumeration
            for the predicate.

        Returns:
            None

        Raises:
            ValueError: If a symbol already exists with the same name
                or if the arguments contains an unknown type.
        """
        if symbol_name in self.symbols:
            raise ValueError(f'Duplicate symbol name: {symbol_name}')
        if isinstance(arguments, str):
            arguments = [arguments]
        self.verify_types(arguments)

        self.symbols[symbol_name] = IDPPredicate(symbol_name, arguments,
                                                 enumeration)

    def function(self, symbol_name: str, arguments: List, returntype: str,
                 enumeration: Dict = {}, default_val: Union[str, int] = None
                 ) -> None:
        """ Add new function to the KB.

        A function is defined by its name, its arguments, a return type,
        (optionally) an enumeration, and (optionally) a default enumeration
        value. Both the arguments and the return type should be valid types.

        Note:
            If an enumeration is used, it **must** either be complete, or a
            default value should be given.
        Args:
            symbol_name (str)
            arguments (`list` of str): The types of the function.
            returntype (str): The type of the return value.
            enumeration (`list` of str or int, optional): Optional enumeration
                for the function.
            default_val (str or int, optional): Optional default value for the
                enumeration.

        Returns:
            None

        Raises:
            ValueError: If a symbol already exists with the same name
                or if the arguments contains an unknown type.
        """
        # if symbol_name in self.symbols:
        #     raise ValueError(f'Duplicate symbol name: {symbol_name}')
        if isinstance(arguments, str):
            arguments = [arguments]
        self.verify_types(arguments + [returntype])

        self.symbols[symbol_name] = IDPFunction(symbol_name, arguments,
                                                returntype, enumeration,
                                                default_val)

    def proposition(self, symbol_name: str, value: bool = None) -> None:
        """ Add new proposition to the KB.

        A proposition is defined by its name, and an optional boolean value.

        Args:
            symbol_name (str)
            value (bool, optional)

        Returns:
            None

        Raises:
            ValueError: If a symbol already exists with the same name.
        """
        if symbol_name in self.symbols:
            raise ValueError(f'Duplicate symbol name: {symbol_name}')
        self.symbols[symbol_name] = IDPProposition(symbol_name, value)

    boolean = proposition

    def constant(self, symbol_name: str, returntype: str,
                 value: Union[str, int] = None) -> None:
        """ Add new constant the KB.

        A constant is defined by its name, its returntype, and (optionally) its
        value. The return type of the constant should already exist.

        Args:
            symbol_name (str)
            returntype (str)
            value (str or int, optional)

        Returns:
            None

        Raises:
            ValueError: If a symbol already exists with the same name
                or if the return type is unknown.
        """
        if symbol_name in self.symbols:
            raise ValueError(f'Duplicate symbol name: {symbol_name}')
        self.verify_types([returntype])

        self.symbols[symbol_name] = IDPConstant(symbol_name, returntype, value)

    def verify_types(self, arguments: List[str]) -> None:
        """ Verifies whether types are already known to the KB

        Args:
            arguments (`list` of str): the types to verify.

        Returns:
            None

        Raises:
            ValueError: if the type does not yet exist.

        """
        return 
        for arg in arguments:
            if arg not in self.symbols.keys():
                raise ValueError(f'Type {arg} does not exist')

    def set_enumeration(self, symbol_name: str, enum: any) -> None:
        """ Set a symbol's enumeration.

        Args:
            symbol_name (str)
            enum (any): the enumeration. Can be str, int, list, dict, ...

        Returns:
            None

        Raises:
            ValueError: if the symbol does not exist, or if the
                type of the enumeration is wrong.
        """
        if symbol_name not in self.symbols:
            raise ValueError(f'Unknown symbol: {symbol_name}')

        symbol_type = self.symbols[symbol_name].symbol_type
        if symbol_type == 'type':
            self.symbols[symbol_name].enumeration = enum
        elif self.symbols[symbol_name].arity > 0:
            # Check if the enum is of the correct type.
            if symbol_type == 'predicate' and not isinstance(enum, List):
                raise ValueError(f'Type of enumeration should be List, not'
                                 f' {type(enum)}')
            elif symbol_type != 'predicate' and not isinstance(enum, dict):
                raise ValueError(f'Type of enumeration should be Dict, not'
                                 f' {type(enum)}')

            self.symbols[symbol_name].enumeration = enum
        else:
            rtype = self.symbols[symbol_name].rtype
            # Check if the enum is of the correct type.
            if rtype == 'Bool' and not isinstance(enum, bool):
                raise ValueError(f'Type of enumeration should be Bool, not'
                                 f'{type(enum)}')
            elif rtype != 'Bool' and not isinstance(enum, (str, int, float)):
                raise ValueError(f'Type of enumeration should be str, int or'
                                 f' float, not {type(enum)}')

            self.symbols[symbol_name].value = enum

    def clear_enumeration(self, symbol_name: str) -> None:
        """ Clear a symbol's enumeration.

        Args:
            symbol_name (str)

        Returns:
            None

        Raises:
            ValueError: if the symbol does not exist.
        """
        if symbol_name not in self.symbols:
            raise ValueError(f'Unknown symbol: {symbol_name}')
        arity = self.symbols[symbol_name].arity
        if arity > 0:
            self.symbols[symbol_name].enumeration = None
        else:
            self.symbols[symbol_name].value = None

    def axiom(self, axiom: str) -> None:
        """ Add a axiom to the KB

        Note:
            This axiom should already be in FO(.) format.

        Args:
            axiom (str): the axiom in FO(.) format.

        Returns:
            None
        """

        self.axioms.append(IDPConstraint(axiom))

    def definition(self, rules: List[str]) -> None:
        """ Add a definition to the KB

        Note:
            The definition rules should already be in FO(.) format.

        Args:
            rules (`list` of str): the rules in FO(.) format.

        Returns:
            None
        """
        self.axioms.append(IDPDefinition(rules))

    def generate_voc(self) -> str:
        """ Generates a vocabulary based on the info in the KB.

        Iterates over all symbols (types, predicates, functions, propsitions
        and constants) and formats them for the vocabulary.

        Args:
            None

        Returns:
            str: the vocabulary
        """
        voc = "vocabulary { \n"
        for symbol in self.symbols.values():
            voc += symbol.to_voc() + "\n"
        voc += "}\n\n"
        return voc

    def generate_theory(self) -> str:
        """ Generates a theory based on the info in the KB.

        Iterates over all axioms and definitions, and formats them in a
        theory.

        Args:
            None

        Returns:
            str: the theory
        """
        theo = "theory { \n"
        for axiom in self.axioms:
            theo += axiom.to_theory() + "\n"
        for definition in self.definitions:
            theo += definition.to_theory() + "\n"
        theo += "}\n\n"
        return theo

    def generate_structure(self) -> str:
        """ Generates a structure based on the info in the KB.

        Iterates over all symbols (types, predicates, functions, propsitions
        and constants) and formats them for the vocabulary.

        Args:
            None

        Returns:
            str: the structure
        """
        struct = "structure { \n"
        for symbol in self.symbols.values():
            struct += symbol.to_structure() + "\n"
        struct += "}\n\n"
        return struct

    def generate_KB(self, inf: str) -> str:
        """ Generate a full fledged IDP knowledge base.

        Note:
            The content of the `main` block should be supplied.

        Args:
            inf (str): the contents of the KB's `main`, in IDP format.

        Returns:
            str: the IDP knowledge base
        """
        KB = self.generate_voc() + self.generate_structure() \
            + self.generate_theory() \
            + f"procedure main() {{\n\t{inf} \n}}"
        return KB

    def model_expand(self, max: int = 10, timeout: int = 0) -> List[Dict]:
        """ Perform model expansion on the KB.

        Args:
            max (int, optional): the max number of models to generate.
            timeout (int, optional): a timeout for IDP-Z3 (in seconds) after
                which results are returned.

        Returns:
            `list` of `dict`: all generated models
        """
        inf = (f'pretty_print(model_expand(T, S, max={max},'
               f' timeout={timeout}))')
        KB = self.generate_KB(inf)
        output = self.execute_KB(KB)
        return self.interpret_model(output)

    def optimize(self, term: str, minimize: bool = True) -> List[Dict]:
        """ Perform optimization (max or min) on the KB.

        Args:
            term (str): the optimization term
            minimize (bool, optional)

        Returns:
            `list` of `dict`: all generated models
        """

        KB = self.generate_KB(f'pretty_print(Theory(T, S)'
                              f'.optimize("{term}", {minimize}))')
        output = self.execute_KB(KB)
        return self.interpret_model(output)

    def minimize(self, term: str) -> List[Dict]:
        """ Perform minimization on the KB.

        Args:
            term (str): the minimzation term

        Returns:
            `list` of `dict`: all generated models
        """
        return self.optimize(term, minimize=True)

    def maximize(self, term: str) -> None:
        """ Perform maximization on the KB.

        Args:
            term (str): the maximization term

        Returns:
            `list` of `dict`: all generated models
        """
        return self.optimize(term, minimize=False)

    def propagate(self) -> None:
        """ Perform propagation on the KB.

        Args:
            None

        Returns:
            `tuple` of `dict`: the postitive facts, and the negative facts
        """
        KB = self.generate_KB('pretty_print(model_propagate(T, S))')
        output = self.execute_KB(KB)
        return self.interpret_prop(output)

    def explain(self) -> str:
        """ Explain an unsat KB.

        Args:
            None

        Returns:
            str: the explanation as generated by IDP-Z3
        """
        KB = self.generate_KB('pretty_print(Theory(T,S).explain())')
        output = self.execute_KB(KB)
        return output

    def custom_main(self, main: str) -> str:
        """ Execute a custom main.

        Note:
            The output of the IDP system is returned directly -- parsing it is
            up to the user of this function.

        Args:
            main (str)

        Returns:
            `str`: IDP's output
        """
        KB = self.generate_KB(main)
        return self.execute_KB(KB)

    def execute_KB(self, KB: str) -> str:
        """ Executes an IDP KB in string format.

        Args:
            KB (str): the KB in string format.

        Returns:
            `str`: IDP's output.
        """
        idp = IDP.from_str(KB)
        # Execute IDP, capture the output.
        with open('/tmp/idp_temp.txt', mode='w', encoding='utf-8') \
                as buf, redirect_stdout(buf):
            idp.execute()
        with open('/tmp/idp_temp.txt', mode='r', encoding='utf-8') as fp:
            output = fp.read()
        return output

    def interpret_model(self, output: str) -> List[Dict]:
        """ Interprets models in IDP's output.

        Mainly to be used after model expansion or optimization.

        Args:
            output (str): IDP's output to parse.

        Returns:
            `list` of `dict`: a list containing all models (as dict)

        """
        # Here be regex.
        models = []
        if output.startswith('No models.'):
            return []
        if 'Model' in output:
            outp_models = re.findall(r'Model \d+\n==========\n(.*?)\n\n',
                                     output, re.DOTALL)
        else:
            outp_models = [output]
        for outp_model in outp_models:
            assignments = outp_model.split('\n')
            model = {}
            for assignment in assignments:
                if assignment == '':
                    continue
                symbol_name, values = assignment.split(' := ')
                values = values.strip('{}')
                symbol_type = self.symbols[symbol_name].symbol_type
                symbol_arity = self.symbols[symbol_name].arity


                # Based on the symbol's type, we interpret the values
                # differently
                if symbol_type == 'proposition':
                    if values == 'true':
                        model[symbol_name] = True
                    else:
                        model[symbol_name] = False
                elif symbol_type == 'constant':
                    model[symbol_name] = values
                elif symbol_type == 'predicate':
                    # Find all values (e.g., (x, y, z)).
                    if '(' in values:
                        regstr = r'\((.*?)\)(, |$)'
                    else:
                        regstr = r'(.*?)(, |$)'

                    values = re.findall(regstr, values)

                    out_values = []
                    # Second var is EOL or `.`. No need to use it.
                    for arg, _ in values:
                        if ',' in arg:
                            arg = tuple(arg.split(','))
                        else:
                            arg = arg.strip()
                        if arg:
                            out_values.append(arg)
                    model[symbol_name] = out_values
                else:  # Function
                    regstr = r'\(?(.*?)\)?->(.*?)(, |$)'
                    values = re.findall(regstr, values)
                    out_values = {}
                    # Third var is EOL or `.`. No need to use it, hence the _
                    for arg, value, _ in values:
                        if ',' in arg:
                            arg = tuple(arg.split(','))
                        else:
                            arg = arg.strip()
                        out_values[arg] = value
                    model[symbol_name] = out_values
            models.append(model)
        return models

    def interpret_prop(self, output: str) -> Tuple[Dict]:
        """ Interpret the propagation output of IDP.

        In short, we collect all output in two dictionaries: one containing
        positive assignments, and one containing the negative assignments.

        Args:
            output (str): IDP's propagation output.

        Returns:
            `tuple` of `dict`: The dicts containing the postitive and negative
                assignments.
        """
        pos_assignments = {}
        neg_assignments = {}
        for symbol_name, symbol in self.symbols.items():
            if symbol.symbol_type == 'type':
                continue
            elif symbol.symbol_type == 'proposition':
                pos_assignments[symbol_name] = None
                neg_assignments[symbol_name] = None
            elif symbol.symbol_type in ['predicate']:
                pos_assignments[symbol_name] = []
                neg_assignments[symbol_name] = []
            elif symbol.symbol_type in ['function']:
                pos_assignments[symbol_name] = {}
                neg_assignments[symbol_name] = {}
            else:
                # In case of constant
                pos_assignments[symbol_name] = None
                neg_assignments[symbol_name] = []

        for line in output.split('\n'):
            # Last two lines are "No more consequences \n", we skip those.
            # neg, symbol_name, args, _, val = re.findall(r'(Not |)(.*?)\((.*?)\)( -> (.*?)$|)', line)[0]
            try:
                neg, symbol_name, args, _, val = re.findall(r'(?:(Not)\s)?(.*?)\((.*?)\)(?:\s(=|->)\s*(\w*))?', line)[0]
            except IndexError:
                continue
            symbol_type = self.symbols[symbol_name].symbol_type
            if ',' in args:
                arg = tuple(args.split(','))
            else:
                arg = args
            if neg:
                assignments = neg_assignments
            else:
                assignments = pos_assignments

            if symbol_type in ['proposition']:
                assignments[symbol_name] = True
            elif symbol_type in ['predicate']:
                assignments[symbol_name].append(arg)
            elif symbol_type in ['function']:
                if neg:
                    # If we're making the neg dict, keep a list of negative
                    # values.
                    if arg in assignments[symbol_name]:
                        assignments[symbol_name][arg].append(val)
                    else:
                        assignments[symbol_name][arg] = [val]
                else:
                    # For pos dict, we only need to store one value.
                    assignments[symbol_name][arg] = val
            else:
                if neg:
                    assignments[symbol_name].append(val)
                else:
                    assignments[symbol_name] = val
        return pos_assignments, neg_assignments


class IDPSymbolObject():
    """ Parent class for all IDP symbols.

    This class contains some attributes and methods shared bt all IDP symbols.

    Attributes:
        symbol_name (str)
    """
    def __init__(self, name: str):
        """ Initialize the symbol object.

        Args:
            name (str)

        Raises:
            ValueError: if the name is invalid.
        """
        self.check_name_validity(name)
        self.symbol_name = name

    def to_voc(self) -> str:
        """ Dummy function """
        return ''

    def to_theory(self) -> str:
        """ Dummy function """
        return ''

    def to_structure(self) -> str:
        """ Dummy function """
        return ''

    def check_name_validity(self, name: str) -> None:
        """ Checks if a name is valid.

        Valid names can only contain alfanumerical characters + dashes,
        underscores, and backticks.

        Args:
            name (str)

        Returns:
            None

        Raises:
            ValueError: if the name is invalid.
        """
        if len(re.findall(r'`?\w+', name)) != 1:
            raise ValueError(f'Invalid name: {name}')
        elif re.match(r'\d', name):
            raise ValueError(f'Invalid name: {name}'
                             f' (cannot start with number)')

    def check_argument_validity(self, args: List[str]) -> None:
        """ Check if the arguments are valid

        Args:
            args (`list` of str)

        Returns:
            None

        Raises:
            ValueError: If args is not a list
        """
        if type(args) is not list:
            raise ValueError(f'Invalid argument(s) "{args}":'
                             f' arguments must be list')
        # TODO: maybe some type checking? I.e. are the arguments valid types?

    def check_enumeration_validity(self, args: List[str], enumeration:
                                   List[Tuple]) -> None:
        """ Check if the enumeration is valid w.r.t the arity

        Args:
            args (`list` of str): the arguments of the symbol
            enumeration (`list` of `tuple`)

        Returns:
            None

        Raises:
            ValueError: If the enumeration has an incorrect number of
                arguments.
        """
        if self.rtype != 'Bool' and self.arity > 1:
            keys = enumeration.keys()
        elif self.arity > 0:
            keys = enumeration
        else:
            keys = []

        if len(args) > 1:
            # Check if the enumeration is correct w.r.t. the arity
            arity = len(args)
            for key_tuple in keys:
                if type(key_tuple) is not tuple or len(key_tuple) != arity:
                    raise ValueError(f'Incorrect number of args for'
                                     f' {self.symbol_name}: {key_tuple}')
        elif len(args) == 1:
            for key in keys:
                if type(key) not in [int, str, float]:
                    raise ValueError(f'Incorrect number of args for'
                                     f' {self.symbol_name}: {key}')

        # TODO: maybe value checking? I.e. are the values correct, given the
        # type?

        # TODO: verify whether enumeration is complete OR has a default value.


class IDPType(IDPSymbolObject):
    """ Class representing an IDP type.

    IDP types consist of two components: a name, and an enumeration.

    Attributes:
        symbol_name (str)
        enumeration (`list` of str/int)
    """
    def __init__(self, name: str, enumeration: Union[List, range]):
        """ Initialize the Type.

        Args:
            name (str)
            enumeration (`list` or `range`)

        Raises:
            ValueError: if the enumeration is of incorrect type.

        """
        super().__init__(name)
        self.symbol_type = 'type'

        if type(enumeration) not in [range, list]:
            raise ValueError(f'Enumeration for {name} should be list or range')

        self.enumeration = [str(x) for x in enumeration]

    def to_voc(self) -> str:
        """ Convert the Type into its vocabulary format.

        Args:
            None

        Returns:
            str: the Type converted into Vocabulary format.
        """
        if type(self.enumeration) is range:
            return (f"\ttype {self.symbol_name} :="
                    f" {{{str(self.enumeration.start)}"
                    f"..{self.enumeration.stop}}}")
        else:
            return (f"\ttype {self.symbol_name} :="
                    f" {{{', '.join(self.enumeration)}}}")

class IDPStandardType(IDPType):
    """ Class representing a built-in IDP type. These have no enumeration.

    IDP types consist of two components: a name.

    Attributes:
        symbol_name (str)
    """
    def __init__(self, name: str):
        """ Initialize the Type.

        Args:
            name (str)

        """
        super().__init__(name, [])
        self.symbol_type = 'type'

    def to_voc(self) -> str:
        """ Built-in types are not added to vocabulary.

        Returns:
            str: empty str
        """
        return ''


class IDPFunction(IDPSymbolObject):
    """ Class representing an IDP function.

    IDP functions consist of five components:
        * a name;
        * its arguments;
        * its return type;
        * (optional) the enumeration;
        * (optional) the default value.

    Attributes:
        symbol_type (str)
        arguments (`list` of str)
        rtype (str): the returntype
        enumeration (`list` of `tuple`)
        arity (int)
        default_val (str)

    """
    def __init__(self, symbol_name: str, arguments: List[str],
                 returntype: str, enumeration: Dict = {}, default_val=None):
        super().__init__(symbol_name)
        self.symbol_type = 'function'

        self.arguments = arguments
        self.rtype = returntype
        self.enumeration = enumeration
        self.arity = len(self.arguments)
        self.default_val = default_val

        self.check_argument_validity(arguments)
        self.check_enumeration_validity(arguments, enumeration)

    def to_voc(self) -> str:
        """ Convert the Function into vocabulary format.

        Args:
            None

        Returns:
            str: the Function in vocabulary format.
        """
        return (f'\t{self.symbol_name} : ({" * ".join(self.arguments)})'
                f' -> {self.rtype}')

    def to_structure(self) -> str:
        """ Convert the Function's enumeration into structure format.

        Args:
            None

        Returns:
            str: the Function's enumeration in structure format.
        """
        if self.enumeration is None or len(self.enumeration) == 0:
            return ''

        assignments = []
        if self.arity > 1:
            for key_tuple, value in self.enumeration.items():
                key_tuple = [str(x) for x in key_tuple]
                assignments.append(f'({", ".join(key_tuple)}) -> {value}')
        elif self.arity == 1:
            for key, value in self.enumeration.items():
                key = str(key)
                assignments.append(f'({key}) -> {value}')
        struct = f'\t{self.symbol_name} := {{{", ".join(assignments)}}}'
        if self.default_val:
            struct += f' else {self.default_val}'
        return struct


class IDPConstant(IDPFunction):
    """ Class representing an IDP constant.

    IDP constants consist of three components:
        * a symbol_name,
        * its return type;
        * (optionally) its value.

    Attributes:
        symbol_type (str)
        arguments (`list` of str)
        rtype (str): the returntype
        enumeration (`list` of `tuple`)
        arity (int)
        default_val (str)

    """
    def __init__(self, symbol_name: str, returntype: str,
                 value=None):
        super().__init__(symbol_name, [], returntype, {(): value})
        self.symbol_type = 'constant'
        self.value = value

    def to_structure(self) -> str:
        """ Convert the Constant into IDP format.
        Args:
            None

        Returns:
            str
        """
        if self.value:
            return f'\t{self.symbol_name} := {self.value}'
        else:
            return ''


class IDPPredicate(IDPFunction):
    """ Class representing an IDP predicate.

    IDP predicates consist of three components:
        * a name;
        * arguments;
        * an enumeration.

    Attributes:
        name (str)
        symbol_type (str)
        enumeration (`list` or `range`)
        arguments (`list` of str)
        arity (int)
    """
    def __init__(self, symbol_name, arguments, enumeration):
        super().__init__(symbol_name, arguments, 'Bool', enumeration)
        self.symbol_type = 'predicate'

    def to_structure(self) -> str:
        if not self.enumeration or len(self.enumeration) == 0:
            return ''
        assignments = []
        if self.arity > 1:
            for key_tuple in self.enumeration:
                key_tuple = [str(x) for x in key_tuple]
                assignments.append(f'({", ".join(key_tuple)})')
        elif self.arity == 1:
            for key in self.enumeration:
                assignments.append(f'({key})')
        return f'\t{self.symbol_name} := {{{", ".join(assignments)}}}'


class IDPProposition(IDPPredicate):
    """ Class representing an IDP proposition.

    IDP propositions consist of three components:
        * a name;
        * arguments;
        * a value.

    Attributes:
        name (str)
        symbol_type (str)
        enumeration (`list` or `range`)
        arguments (`list` of str)
        arity (int)
        value
    """
    def __init__(self, symbol_name, value):
        super().__init__(symbol_name, [], [value])
        self.symbol_type = 'proposition'
        self.value = value

    def to_structure(self) -> str:
        """ Convert the Proposition into its structure format.

        Args:
            None

        Returns:
            str: the Proposition in its structure format
        """
        if self.value is None:
            return ''
        elif self.value:
            return f'\t{self.symbol_name} := true'
        else:
            return f'\t{self.symbol_name} := false'


class IDPConstraint():
    """ Class representing the IDP axiom.

    Attributes:
        axiom (str): the constrain in FO(.) format.

    """
    def __init__(self, axiom):
        if axiom.endswith('.'):
            self.axiom = axiom
        else:
            self.axiom = f'{axiom}.'

    def to_theory(self) -> str:
        """" Returns the axiom

        Return:
            str
        """
        return f'\t{self.axiom}\n'


class IDPDefinition():
    """ Class representing the IDP definition.

    Attributes:
        rules (`list` of str)
    """
    def __init__(self, rules):
        self.rules = []
        for rule in rules:
            if not rule.endswith('.'):
                self.rules.append(rule + '.')
            else:
                self.rules.append(rule)

    def to_theory(self) -> str:
        """ Returns the definition in FO(.) format.

        Returns:
            str
        """
        return '\t{\n\t\t' + '\n\t\t'.join(self.rules) + '\n\t}\n'
