# IDP GRPC

"IDP as a service"

The core idea is to generate an IDP GRPC specification for specific IDP theories.
In short, you can send an IDP theory to the IDPGenerator, which will generate (a) a custom proto file and (b) spin up a specific IDP GPRC server.

## Contents

* theory2proto.py: convert an IDP theory into a protofile
* IDPservice.py: contains the code for the IDP GRPC server
* server.py: implements the IDPGenerator, which creates the protofiles and launches the IDP GRPC server
* client.py: an example implementation of a possible client


## IDP GRPC features

The IDP server (in `IDPservice.py`) supports following methods:

* modelExpand
* propagate
* explain
* getVariables
* setStructure
* getStructure


See below for example usage.


## Installation

This project requires some additional Python packages. These can be installed through pip as follows:

```
pip3 install -r requirements.txt
```

Afterwards, generate the IDP Generator's modules based on the protofile supplied in this repo:

```
python -m grpc_tools.protoc -I./protos --python_out=. --grpc_python_out=. idpgenerator.proto
```


## Using the example

The example can be executed by running the server and client in separate terminals.

```
python server.py
```

```
python client.py
```



## Code snippets of features

Here, `gen_pb2` is the Python file generated on the protofiles sent from the server.

#### modelExpand

```
# First, create request message.
request = gen_pb2.ExpandRequest(nb_models=3, timeout=0)

# Then perform model generation.
response = stub.modelExpand(request)
```

#### propagate

```
response = stub.propagate(gen_pb2.PropRequest())
```

#### explain

For explanation to work, IDP's structure needs to be unsat.

```
# Set an unsat partial structure.
partial_struc = gen_pb2.Model()
partial_struc.convex.value = True
stub.setStructure(partial_struc)

stub.explain(gen_pb2.Empty())
```

#### getVariables

This method returns 3 maps:

* Map of variables on their logical type (e.g. `{'convex': 'proposition'}`)
* Map of variables on their typing of arguments (e.g. `{'angle': 't_side_nr'}`)
* Map of variables on their return type (e.g. `{'subtype': 't_subtype'}`)

```
response = stub.getVariables(gen_pb2.Empty())
```

#### setStructure & getStructure

getStructure allows for fetching the KB's current structure, whereas setStructure updates it.

It is possible to update the remote KB's entire structure, by first creating a Model object.

```
partial_struc = gen_pb2.Model()
partial_struc.convex.value = True
stub.setStructure(partial_struc)
```

Alternatively, it is also possible to first request the current structure, to modify this, and then send it back.

```
struc = stub.getStructure(gen_pb2.Empty())
struc.convex.value = True
struc.length.value['4'] = '10'
stub.setStructure(struc)
```

