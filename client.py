import grpc
import idpgenerator_pb2 as pb2
import idpgenerator_pb2_grpc as pb2_grpc
import time
import sys
import logging
from grpc_tools import protoc


theory = """
vocabulary {

    type t_nr_of_sides := {3..4}
    nr_of_sides: () -> t_nr_of_sides
    nr_of_vertices: () → t_nr_of_sides


    type t_type := {triangle, quadrilateral}
    type : () → t_type

    type t_side_nr := {1..4}
    length, angle : (t_side_nr) → ℝ

    type t_subtype :=
        { regular_triangle
        , right_triangle
        , rectangle
        , square
        , irregular}
    subtype : () → t_subtype

    convex, equilateral : () → 𝔹
    perimeter: () → ℝ


    another_pred: (t_side_nr * t_side_nr) -> Bool
    another_func: (t_nr_of_sides * t_side_nr * t_subtype) -> Int

    // base : () → ℝ
    // height: () → ℝ
    // surface:() → ℝ
}

theory {
    [Definition of triangle]
    type()=triangle      ⇔ nr_of_sides()=3.

    [Definition of rectangle]
    type()=quadrilateral ⇔ nr_of_sides()=4.

    [There are as many vertices as there are sides]
    (nr_of_vertices()=3 ⇔ nr_of_sides()=3) ∧
    (nr_of_vertices()=4 ⇔ nr_of_sides()=4).

    [All triangles are convex]
    nr_of_sides()=3 ⇒ convex().

    [Definition of convex]
    convex() ⇔ [All angles are below 180°]
               ∀n ∈ t_side_nr: angle(n)<180.

    [Definition of equilateral]
    equilateral() ⇔ [All sides have the same length]
                    ∀n ∈ t_side_nr: length(n)=length(1) ∨ length(n)=0.

    [A triangle is regular, right, or irregular]
    subtype()=right_triangle ∨ subtype()=regular_triangle ∨ subtype()=irregular ⇐ nr_of_sides()=3.

    [A quadrilateral is a square, a rectangle, or irregular]
    subtype()=square ∨ subtype()=rectangle ∨ subtype()=irregular ⇐ nr_of_sides()=4.

    [Squares and rectangles are quadrilaterals]
    subtype()=square ∨ subtype()=rectangle ⇒ nr_of_sides()=4.

    [A regular triangle is an equilateral triangle]
    subtype()=regular_triangle ⇔ nr_of_sides()=3 ∧ equilateral().

    [A regular triangle is a triangle where all angles are 60°]
    subtype()=regular_triangle ⇔ nr_of_sides()=3 ∧
                                 [All angles are 60°]
                                 ∀n ∈ t_side_nr: angle(n)=60 ∨ angle(n)=0.

    [A right triangle is a triangle with a 90° angle]
    subtype()=right_triangle   ⇔ nr_of_sides()=3 ∧
                                 [One angle is 90°]
                                 ∃n ∈ t_side_nr: angle(n)=90.

    [All angles are 90° in squares and rectangles]
    nr_of_sides()=4 ∧ subtype()≠irregular ⇔
            [All angles are 90°]
            ∀n ∈ t_side_nr: angle(n)=90∨ angle(n)=0.

    [In a rectangle, opposite Side_Nr have the same length, and adjacent sides have different lengths]
    subtype()=rectangle ⇒ length(3)=length(1) ∧ length(2)=length(4) ∧ length(2)≠length(1).

    [A square is an equilateral quadrilateral]
    subtype()=square    ⇒ nr_of_sides()=4 ∧ equilateral().

    [In a triangle, no Side_Nr is longer than the sum of the 2 other sides]
    type()=triangle ⇒ ( length(1) ≤ length(2) + length(3))
                    ∧  ( length(2) ≤ length(3) + length(1))
                    ∧  ( length(3) ≤ length(1) + length(2)).

    // General constraints

    [The perimeter is the sum of the lengths of the sides]
    perimeter() = sum(lambda n ∈ t_side_nr : length(n)).

    [The sum of the angles is 180° for a triangle, and 360° for a quadrilateral]
    sum(lambda n in t_side_nr : angle(n)) = (nr_of_sides()-2)*180.

    [Lengths are positive numbers]
    ∀x ∈ t_side_nr: 0 ≤ length(x).

    [Angles are positive numbers]
    ∀x ∈ t_side_nr: 0 ≤ angle(x).

    [Angles cannot be 180°]
    ∀x ∈ t_side_nr: angle(x) ≠ 180.

    [A polygon with N vertices has N angles]
    ∀n ∈ t_side_nr: angle(n)=0 ⇔ nr_of_vertices()<n.

    [A polygon with N sides has N lengths]
    ∀n ∈ t_side_nr: length(n)=0 ⇔ nr_of_sides()<n.

    nr_of_sides() = 3.
}
structure {
}
"""


def run():
    # First, we need to set up the IDP GRPC server for this theory.
    # We do this by sending our theory to the generator server,
    # which returns a proto file and a port number.
    proto, port = setup_IDP()
    time.sleep(1)

    # Then we generate the proto modules, based on the received file.
    # However, the server also generates these in this folder, so this step is
    # skipped.
    # path = ''
    # idir = '-I./protos'
    # out = '--python_out=.'
    # grpc_out = '--grpc_python_out=.'
    # name = 'gen.proto'
    # protoc.main([path, idir, out, grpc_out, name])

    # Then we import the generated proto and connect to the IDP GRPC server.
    import gen_pb2
    import gen_pb2_grpc

    with grpc.insecure_channel(f'localhost:{port}') as channel:
        stub = gen_pb2_grpc.IDPStub(channel)

        # Set an initial structure.
        partial_struc = gen_pb2.Model()
        partial_struc.convex.value = True
        stub.setStructure(partial_struc)

        # Perform model expansion.
        request = gen_pb2.ExpandRequest(nb_models=2, timeout=0)
        response = stub.modelExpand(request)

        # Request all variables present in the theory, and iterate over them
        # to nicely print out our model.
        variables = stub.getVariables(gen_pb2.Empty())
        var_list = list(variables.type.keys())
        print('Performed model expansion:')
        print('=======================================')
        for model in response.models:
            for var in var_list:
                if model.HasField(var):
                    value = getattr(model, var).value
                else:
                    value = 'None'
                print(f'{var}: {value}')
            print('=======================================')

        # Perform propagation.
        print('Propagation:')
        print(stub.propagate(gen_pb2.PropRequest()))

        # Set a partial structure that is unsat, and ask an explanation.
        partial_struc = stub.getStructure(gen_pb2.Empty())
        partial_struc.convex.value = True
        partial_struc.length.value['4'] = '10'
        stub.setStructure(partial_struc)
        print('Explanation:')
        print(stub.explain(gen_pb2.Empty()))


def setup_IDP():
    print('connecting to meta')
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = pb2_grpc.IDPGeneratorStub(channel)
        response = stub.generateProto(pb2.Theory(theory=theory))
        return(response.proto, response.port)


if __name__ == '__main__':
    logging.basicConfig()
    print('starting client')
    run()
