# Create proto file for specific IDP-Z3 theory.
from idp_engine.API import KB


def theory_to_proto(theory):
    service = """syntax = "proto3";
    package IDPGRPC;
    service IDP {

        rpc modelExpand (ExpandRequest) returns (Models) {}

        rpc propagate (PropRequest) returns (PropModels) {}

        rpc explain (ExplainRequest) returns (Explain) {}

        rpc setStructure (Model) returns (Ok) {}

        rpc getStructure (Empty) returns (Model) {}

        rpc getVariables (Empty) returns (VarList) {}

    }"""

    msgs = []
    msg_ok = """message Ok {
        bool success = 1;
        Errors errors = 2;
    }"""
    msgs.append(msg_ok)
    msg_empty = """message Empty {
    }"""
    msgs.append(msg_empty)
    msg_str = """message StrValue {
        string value = 1;
    }"""
    msgs.append(msg_str)
    msg_error = """message Errors {
        repeated string errors = 1;
    }"""
    msgs.append(msg_error)
    # msg_int = """message IntValue {
    #     int64 value = 1;
    # }"""
    # msgs.append(msg_int)
    # msg_float = """message FloatValue {
    #     float value = 1;
    # }"""
    # msgs.append(msg_float)
    msg_type = """message TypeValue {
        repeated string value = 1;
    }"""
    msgs.append(msg_type)
    msg_bool = """message BoolValue {
        bool value = 1;
    }"""
    msgs.append(msg_bool)
    msg_map = """message MapValue {
        map<string, string> value = 1;
    }"""
    msgs.append(msg_map)
    msg_repeated = """message RepeatedValue {
        repeated string value = 1;
    }"""
    msgs.append(msg_repeated)
    msg_varlist = """message VarList {
        map<string, string> type = 1;
        map<string, string> args = 2;
        map<string, string> rtype = 3;
    }"""
    msgs.append(msg_varlist)
    msg_expand = """message ExpandRequest {
        int64 nb_models = 1;
        int64 timeout = 2;
    }"""
    msgs.append(msg_expand)
    msg_prop = """message PropRequest {
    }"""
    msgs.append(msg_prop)
    msg_explain = """message ExplainRequest {
        Model model = 1;
    }"""
    msgs.append(msg_explain)
    msg_explanation = """message Explain {
        repeated string explanations = 1;
    }"""
    msgs.append(msg_explanation)
    msg_models = """message Models {
        bool success = 1;
        repeated Model models = 2;
        Errors errors = 3;
    }"""
    msgs.append(msg_models)

    msg_propmodels = """message PropModels {
        Model posModel = 1;
        string negModel = 2;
    }"""
    msgs.append(msg_propmodels)

    msg_model = """message Model {
    """

    idp = KB()
    idp.from_str(theory)

    idx = 1
    for symbol_name, symbol in idp.symbols.items():
        if symbol_name in ['Bool', 'Int', 'Real', 'Date',
                           'Concept']:
            continue
        symbol_type = symbol.symbol_type
        if symbol_type in ['type']:
            msg_model += f'\tTypeValue {symbol_name} = {idx};\n'
        elif symbol_type == 'proposition':
            msg_model += f'\tBoolValue {symbol_name} = {idx};\n'
        elif symbol_type == 'constant':
            msg_model += f'\tStrValue {symbol_name} = {idx};\n'
        elif symbol_type == 'function':
            typ = "MapValue"
            msg_model += f'\t{typ} {symbol_name} = {idx};\n'
        else:  # type == 'predicate'
            typ = "RepeatedValue"
            msg_model += f'\t{typ} {symbol_name} = {idx};\n'
        idx += 1
    msg_model += "}"

    msgs.append(msg_model)
    return service + '\n' + '\n\n'.join(msgs)
